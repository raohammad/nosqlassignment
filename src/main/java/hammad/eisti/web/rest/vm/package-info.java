/**
 * View Models used by Spring MVC REST controllers.
 */
package hammad.eisti.web.rest.vm;
